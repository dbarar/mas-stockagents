package stocksTrading.investors;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;


public class InvestorAgent extends Agent {
    private final int minInitialNet = 100;
    private final int maxInitialNet = 1000;

    private List<AID> stocksAgents = new ArrayList<>();

    // investor characteristics
    private int initialNet;
    private int net;
    private int confidenceToBuy;  //50 - min, 100 - max
    private int confidenceToSell; // 0 - min, 50 - max // when lower
    private Map<String, ValueStockBought> stocksBought = new HashMap<>();

    protected void setup() {
        initialNet = (int) (Math.random() * (maxInitialNet - minInitialNet + 1) + minInitialNet);
        net = initialNet;
        confidenceToBuy = (int) (Math.random() * (100 - 50 + 1) + 50);
        confidenceToSell = (int) (Math.random() * (50 -  0 + 1) +  0);

        System.out.println(this.toString());

        // Register the InvestorsAgents service in the yellow pages
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("stock-investor");
        sd.setName("stock-investor");
        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
        // Add a TickerBehaviour that schedules a request to seller agents every minute
        addBehaviour(new UpdateLocalStockAgentsList(this, 1000));
        addBehaviour(new UpdateConfidence(this, 3000));
    }

    protected void takeDown() {
        try {
            DFService.deregister(this);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
        System.out.println("Terminating..." + this.toString());
    }

    public String toString() {
        return "Investor agent: " + getAID().getName() + "\n" +
                "money = " + initialNet + "\n" +
                "stocksBought = " + stocksBought + "\n" +
                "confidenceToBuy = " + confidenceToBuy + "\n" +
                "confidenceToSell = " + confidenceToSell + "\n" +
                "initialNet = " + initialNet + "\n" +
                "net = " + net + "\n" +
                "stocksValue = " + getStocksValue() + "\n" +
                "profit = " + getProfit() + "\n";
    }

    public int getStocksValue(){
        AtomicInteger stocksValue = new AtomicInteger();
        stocksBought.forEach((k, v) -> stocksValue.addAndGet(v.count * v.price));
        return stocksValue.intValue();
    }

    public int getProfit() {
        return (net + getStocksValue()) - initialNet;
    }

    public int getInitialNet() {
        return initialNet;
    }

    public void setInitialNet(int initialNet) {
        this.initialNet = initialNet;
    }

    public int getNet() {
        return net;
    }

    public void setNet(int net) {
        this.net = net;
    }

    public int getConfidenceToBuy() {
        return confidenceToBuy;
    }

    public void setConfidenceToBuy(int confidenceToBuy) {
        this.confidenceToBuy = confidenceToBuy;
    }

    public Map<String, ValueStockBought> getStocksBought() {
        return stocksBought;
    }

    public void setStocksBought(Map<String, ValueStockBought> stocksBought) {
        this.stocksBought = stocksBought;
    }

    public int getConfidenceToSell() {
        return confidenceToSell;
    }

    public void setConfidenceToSell(int confidenceToSell) {
        this.confidenceToSell = confidenceToSell;
    }

    public List<AID> getStocksAgents() {
        return stocksAgents;
    }

    public void setStocksAgents(List<AID> stocksAgents) {
        this.stocksAgents = stocksAgents;
    }

    private static class ValueStockBought{
        int price;
        int count;
        public ValueStockBought(int price, int count){
            this.count = count;
            this.price = price;
        }

        @Override
        public String toString() {
            return "ValueStockBought{" +
                    "price=" + price +
                    ", count=" + count +
                    '}';
        }
    }

    private static class UpdateLocalStockAgentsList extends TickerBehaviour {
        private InvestorAgent a;

        public UpdateLocalStockAgentsList(Agent a, long period) {
            super(a, period);
            this.a = (InvestorAgent) a;
        }

        @Override
        protected void onTick() {
            DFAgentDescription template = new DFAgentDescription();
            ServiceDescription sd = new ServiceDescription();
            sd.setType("stock-selling");
            template.addServices(sd);
            try {
                DFAgentDescription[] result = DFService.search(myAgent, template);

                List<AID> r = new ArrayList<>();
                for (DFAgentDescription dfAgentDescription : result) {
                    r.add(dfAgentDescription.getName());
                }
                a.setStocksAgents(r);
            } catch (FIPAException fe) {
                fe.printStackTrace();
            }
//            System.out.println("StocksAgents found:" + a.getStocksAgents());
            // Perform the request
            a.addBehaviour(new Trade(a));
        }
    }

    private static class UpdateConfidence extends TickerBehaviour {
        private InvestorAgent a;

        public UpdateConfidence(Agent a, long period) {
            super(a, period);
            this.a = (InvestorAgent) a;
        }

        @Override
        protected void onTick() {
            int newConfidenceToBuy = a.getConfidenceToBuy() + (int) (Math.random() * (10 - (-10) + 1) + (-10));
            newConfidenceToBuy = Math.min(newConfidenceToBuy, 100);
            newConfidenceToBuy = Math.max(newConfidenceToBuy, 50);
            a.setConfidenceToBuy(newConfidenceToBuy);

            int newConfidenceToSell = a.getConfidenceToSell() + (int) (Math.random() * (10 - (-10) + 1) + (-10));
            newConfidenceToSell = Math.min(newConfidenceToSell, 50);
            newConfidenceToSell = Math.max(newConfidenceToSell, 0);
            a.setConfidenceToSell(newConfidenceToSell);

            if(a.getNet() == 0 && a.getStocksValue() == 0)
                a.takeDown();

            System.out.println(a.toString());
        }
    }

    private static class Trade extends CyclicBehaviour {
        private InvestorAgent a;
        private MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);

        public Trade(Agent a) {
            super();
            this.a = (InvestorAgent) a;
        }

        @Override
        public void action() {
            ACLMessage msg = a.receive(mt);
            if (msg != null) {
                String stockAgentUpdate = msg.getContent();
                String[] values = stockAgentUpdate.split(" ");
                int confidence = Integer.parseInt(values[0]);
                int stocks = Integer.parseInt(values[1]);
                int price = Integer.parseInt(values[2]);

                int net = a.getNet();
                Map<String, ValueStockBought> stocksBought = a.getStocksBought();
                String key = msg.getSender().getName();

                ACLMessage reply = msg.createReply();
                // Yes! I want to BUY one stock
                if (confidence > a.getConfidenceToBuy() && stocks > 0 && a.getNet() > price) {
                    if (stocksBought.containsKey(key)) {
                        stocksBought.put(key, new ValueStockBought(price, stocksBought.get(key).count + 1));
                    } else {
                        stocksBought.put(key, new ValueStockBought(price, 1));
                    }
                    reply.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
                    net = net - price;
                    System.out.println("InvestorAgent: " + a.getAID().getName() + " BUY: stocks: " + a.getStocksBought());
                }
                // Nope! I want to SELL one stock back to you if I had bought one
                else if (confidence < a.getConfidenceToSell() && stocksBought.containsKey(key) && stocksBought.get(key).count > 0) {
                    stocksBought.get(key).count--;
                    stocksBought.get(key).price = price;
                    net = net + price;
                    reply.setPerformative(ACLMessage.REJECT_PROPOSAL);
                    System.out.println("InvestorAgent: " + a.getAID().getName() + " SELL: stocks: " + a.getStocksBought());
                }
                // Hmm.. I will HOLD my stocks
                else if (stocksBought.containsKey(key) && stocksBought.get(key).count > 0) {
                    stocksBought.get(key).price = price;
                    reply.setPerformative(ACLMessage.CONFIRM);
                    System.out.println("InvestorAgent: " + a.getAID().getName() + " HOLD: stocks: " + a.getStocksBought());
                }
                // We don't have anything in common
                else {
                    reply.setPerformative(ACLMessage.UNKNOWN);
                    System.out.println("InvestorAgent: " + a.getAID().getName() + " UNKNOWN: stocks: " + a.getStocksBought());
                }
                a.setNet(net);
                a.setStocksBought(stocksBought);
                a.send(reply);
            } else {
                block();
            }
        }
    }
}