package stocksTrading.stocks;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.ArrayList;
import java.util.List;

public class StockAgent extends Agent {
    private final int minNumber = 1000;
    private final int maxNumber = 10000;
    private final int minPrice = 10;
    private final int maxPrice = 100;

    private List<AID> investorAgents = new ArrayList<>();

    private int stocksTotal;
    private int stocksForSale;
    private int stocksForCompany;
    private int stockPrice;
    private int initialCapital;
    private int confidence; // 0 - zero conf, 100 - total confidence

    protected void setup() {
        stocksTotal = (int) (Math.random() * (maxNumber - minNumber + 1) + minNumber);
        stocksForSale = (int) (0.7 * stocksTotal);
        stocksForCompany = stocksTotal - stocksForSale;
        stockPrice = (int) (Math.random() * (maxPrice - minPrice + 1) + minPrice);
        initialCapital = stocksForCompany * stockPrice;
        confidence = (int) (Math.random() * (55 - 45 + 1) + 45);

        System.out.println(this.toString());

        // Register the stocks-selling service in the yellow pages
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("stock-selling");
        sd.setName("stock-selling");
        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }

        // Add Behaviours
        addBehaviour(new UpdateLocalInvestorsAgentsList(this, 1000));
        addBehaviour(new UpdateConfidence(this, 3000));
    }

    protected void takeDown() {
        // Deregister from the yellow pages
        try {
            DFService.deregister(this);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
        System.out.println("Terminating..." + this.toString());
    }

    public String toString() {
        return "\n" +
                "Company agent: " + getAID().getName() + "\n" +
                "initialCapital = " + initialCapital + "\n" +
                "stocksNumber = " + stocksTotal + "\n" +
                "stocksForSale = " + stocksForSale + "\n" +
                "stocksForCompany = " + stocksForCompany + "\n" +
                "stockPrice = " + stockPrice + "\n" +
                "confidence = " + confidence + "\n" +
                "profit = " + getProfit() + "\n";
    }

    public int getProfit() {
        return stocksForCompany * stockPrice - initialCapital;
    }

    public int getStocksTotal() {
        return stocksTotal;
    }

    public void setStocksTotal(int stocksTotal) {
        this.stocksTotal = stocksTotal;
    }

    public int getStockPrice() {
        return stockPrice;
    }

    public void setStockPrice(int stockPrice) {
        this.stockPrice = stockPrice;
    }

    public int getInitialCapital() {
        return initialCapital;
    }

    public void setInitialCapital(int initialCapital) {
        this.initialCapital = initialCapital;
    }

    public int getConfidence() {
        return confidence;
    }

    public void setConfidence(int confidence) {
        this.confidence = confidence;
    }

    public int getStocksForSale() {
        return stocksForSale;
    }

    public void setStocksForSale(int stocksForSale) {
        this.stocksForSale = stocksForSale;
    }

    public List<AID> getInvestorAgents() {
        return investorAgents;
    }

    public void setInvestorAgents(List<AID> investorAgents) {
        this.investorAgents = investorAgents;
    }

    public int getStocksForCompany() {
        return stocksForCompany;
    }

    public void setStocksForCompany(int stocksForCompany) {
        this.stocksForCompany = stocksForCompany;
    }

    private static class UpdateLocalInvestorsAgentsList extends TickerBehaviour {
        private StockAgent a;

        public UpdateLocalInvestorsAgentsList(Agent a, long period) {
            super(a, period);
            this.a = (StockAgent) a;
        }

        @Override
        protected void onTick() {
            DFAgentDescription template = new DFAgentDescription();
            ServiceDescription sd = new ServiceDescription();
            sd.setType("stock-investor");
            template.addServices(sd);
            try {
                DFAgentDescription[] result = DFService.search(myAgent, template);

                List<AID> r = new ArrayList<>();
                for (DFAgentDescription dfAgentDescription : result) {
                    r.add(dfAgentDescription.getName());
                }
                a.setInvestorAgents(r);
            } catch (FIPAException fe) {
                fe.printStackTrace();
            }
//            System.out.println("Investor agents found:" + a.getInvestorAgents());
            // Perform the request
            a.addBehaviour(new Trade(a));
        }
    }

    private static class UpdateConfidence extends TickerBehaviour {
        private StockAgent a;

        public UpdateConfidence(Agent a, long period) {
            super(a, period);
            this.a = (StockAgent) a;
        }

        @Override
        protected void onTick() {
            int newConfidence = a.getConfidence() + (int) (Math.random() * (10 - (-10) + 1) + (-10));
            newConfidence = Math.min(newConfidence, 100);
            newConfidence = Math.max(newConfidence, 0);
            a.setConfidence(newConfidence);
            System.out.println(a.toString());
            // Debatable
            if(a.stocksForSale == 0){
                a.takeDown();
            }
        }
    }

    private static class Trade extends Behaviour {
        private int step = 0;
        private StockAgent a;
        private MessageTemplate mt;

        public Trade(Agent a) {
            super();
            this.a = (StockAgent) a;
        }

        @Override
        public void action() {
            switch (step) {
                case 0:
                    ACLMessage inf = new ACLMessage(ACLMessage.INFORM);
                    for (AID investorAgent : a.getInvestorAgents()) {
                        inf.addReceiver(investorAgent);
                    }
                    inf.setContent(a.getConfidence()
                            + " " + (a.getStocksForSale() - a.getInvestorAgents().size())
                            + " " + a.getStockPrice());
                    inf.setConversationId("stock-price");
                    inf.setReplyWith("inf" + System.currentTimeMillis());
                    a.send(inf);

                    // Prepare the template to get the reply
                    mt = MessageTemplate.and(
                            MessageTemplate.MatchConversationId("stock-price"),
                            MessageTemplate.MatchInReplyTo(inf.getReplyWith()));
                    step = 1;
                    break;
                case 1:
                    ACLMessage reply = a.receive(mt);
                    if (reply != null) {
                        // Huraaah! He wants to BUY
                        if (reply.getPerformative() == ACLMessage.ACCEPT_PROPOSAL) {
                            a.setStocksForSale(a.getStocksForSale() - 1);
                            a.setStockPrice((int) (a.getStockPrice() + a.getStockPrice() * 0.1));
                        }
                        // Oooh no! He wants to SELL
                        else if (reply.getPerformative() == ACLMessage.REJECT_PROPOSAL) {
                            a.setStocksForSale(a.getStocksForSale() + 1);
                            a.setStockPrice((int) (a.getStockPrice() - a.getStockPrice() * 0.1));
                        }
                        // Okay! He wants to HOLD
                        else if (reply.getPerformative() == ACLMessage.CONFIRM) {
                            a.setStockPrice((int) (a.getStockPrice() - a.getStockPrice() * 0.01));
                        }
                    } else {
                        block();
                    }
                    step = 2;
                    break;
            }
        }

        @Override
        public boolean done() {
            return step == 2;
        }
    }
}
